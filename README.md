# api-pipeline-trigger-test

Triggering pipeline through the GitLab API with parameters

See [GitLab-CI documentation](https://docs.gitlab.com/ee/ci/triggers/)

## Install

Using a virtualenv:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

## Configure

Copy sample configuration and adapt to your needs

```
cp .env.example .env
```

## Run

Using [dotenv](https://pypi.org/project/python-dotenv/):

```bash
dotenv run python trigger_pipeline.py --log info <nickname>
```

E.g. running:

```bash
> dotenv run python trigger_pipeline.py --log info 'Scott Tiger'
INFO:__main__:2021-07-01 09:34:41,902:Trigger pipeline with 'Scott Tiger'
INFO:__main__:2021-07-01 09:34:43,044:Triggered pipeline: https://gitlab.com/pierredittgen/pipeline-test/-/pipelines/330096991
```

Job logs ends with:

```log
...
Using docker image sha256:d4ff818577bc193b309b355b02ebc9220427090057b54a59e73b79bdfe139b83 for alpine with digest alpine@sha256:234cb88d3020898631af0ccbbcca9a66ae7306ecd30c9720690858c1b007d2a0 ...
$ echo "Hello, $USER_NICKNAME!"
Hello, Scott Tiger!
Cleaning up file based variables 00:00
Job succeeded
```
